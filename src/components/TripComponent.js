import React from 'react';
import './../bus-schedule.css';

export default class Trip extends React.Component {
    constructor(props) {
        super(props);
            this.handleSelectTrip = this.handleSelectTrip.bind(this);
        }

    handleSelectTrip(event) {
        for(var i = 1; i <= 9; i++) {
            if(i == event.target.id) {
                if(event.target.getAttribute('class') == "trip") {
                    event.target.setAttribute("class", "trip-selected");
                } else {
                    event.target.setAttribute("class", "trip");
                }
            } else {
                document.getElementById(i).setAttribute("class", "trip");
            }
        }
        this.props.onSelectBus(event.target.id, event.target.dataset.bus);
        event.stopPropagation();

    }

    render(props) {
        const styles = {
            container: {
                position: 'absolute',
                top: '5',
                left: this.props.trip.startTime,
                width: this.props.trip.endTime-this.props.trip.startTime,
                maxWidth: this.props.trip.endTime-this.props.trip.startTime
            }
        };
        return (
            <div id={this.props.trip.id} className="trip" data-bus={this.props.bus} style={styles.container} onClick={this.handleSelectTrip}>
                {this.props.trip.id}
            </div>
        );
    }
}
