import React from 'react';
import TRIPS from '../data/bus-scheduling-input.json';
import './../bus-schedule.css';
import Trip from './TripComponent';

export default class Bus extends React.Component {
    constructor(props) {
        super(props);
        var trips = [], allBuses = [], k = 1, k1= 1;

        this.state = {
            selectTrip: 0
        };
        for(var j = 0; j < TRIPS.length; j++) {
            var name = "bus_"+k;
            allBuses.push(name);

            this.state[name] = typeof TRIPS[j] != "undefined" ? [TRIPS[j]]: [];
            trips[k++] = [TRIPS[j]];
        }
        this.state["all_buses"] = allBuses;
        this.state["total_buses"] = allBuses.length;
        this.handleBusSelection = this.handleBusSelection.bind(this);
        this.handleMoveTrip = this.handleMoveTrip.bind(this);
    }

    handleBusSelection( trip_id, bus_id,) {
        this.setState({selectTrip: trip_id});
        this.setState({selectTripBus: bus_id});
        //add a new empty trip if one doesn't exists
        if(this.state.empty_bus == null || typeof this.state.empty_bus == "undefined") {
            var busCount = this.state.total_buses + 1;
            var newBus = {}, emptyBus = {};
            newBus["bus_"+busCount] = [];
            emptyBus["empty_bus"] = "bus_"+busCount;
            this.setState(emptyBus);
            this.setState({total_buses: busCount});
            this.setState(newBus);
            //update allTrips
            var currentBuses  = this.state.all_buses;
            currentBuses.splice(busCount, 0, "bus_"+busCount);
            this.setState({all_buses: currentBuses});
        }
    }


    handleMoveTrip(event) {
        if(this.state.selectTrip == 0 || typeof this.state.selectTrip == "undefined") {
            return;
        }
        var selectTripObj = TRIPS[this.state.selectTrip - 1];

        var name = event.target.attributes.id.value;
        var temp = this.state[name];
        //find overlapping trips
        var foundPosition = -99;
        for(var j = 0; j < temp.length; j++) {
            if(selectTripObj.startTime > temp[j].endTime) {
                foundPosition = j+1;
                continue;
            } else if(selectTripObj.endTime < temp[j].startTime) {
                foundPosition = j;
                continue;
            } else {
                foundPosition = -999;
                break;
            }
        }
        //move trip and cleanup empty buses and newly added empty bus
        if(foundPosition != -999) {
            temp.splice(foundPosition, 0, selectTripObj)
            this.setState({
               [name]: temp
            });
            //remove from previous bus
            var oldBus = this.state[this.state.selectTripBus];
            for(var m = 0; m < oldBus.length; m++) {
                if(oldBus[m].id == this.state.selectTrip) {
                    oldBus.splice(m, 1);
                    if(oldBus.length > 0) {
                        this.setState({
                            [this.state.selectTripBus]: oldBus
                        });
                    } else {
                        delete this.state[this.state.selectTripBus];

                        for(var n = 0; n < this.state.all_buses.length; n++) {
                            if(this.state.all_buses[n] == this.state.selectTripBus) {
                                delete this.state.all_buses[n];
                            }
                        }
                        //remove old bus
                        delete this.state.selectTrip;
                        delete this.state.selectTripBus;

                    }
                }

            }
        }
        //take care of empty trips
        if(typeof this.state.empty_bus != "undefined") {
            if(typeof this.state[this.state.empty_bus] == "undefined" || this.state[this.state.empty_bus].length < 1) {
                //delete all about the empty bus, reduce total buses, delete from state, delete from all_buses
                delete this.state[this.state.empty_bus];
                delete this.state.empty_bus;
                var busCount = this.state.total_buses;
                busCount -=1;
                this.setState({
                    total_buses: busCount
                });

                var currentBuses  = this.state.all_buses;
                currentBuses.splice(busCount, 1);
                this.setState({all_buses: currentBuses});
            } else {
                //empty bus is filled
                delete this.state.empty_bus;
            }
        }
        this.forceUpdate();
        event.preventDefault();
    }

    render() {
        var busCount = 1;
        const indBus = this.state.all_buses.map((bus) => {
            var stripe = 'bus ' + (busCount%2 == 0 ? 'gray-bus': 'white-bus');
            const bus_trips = this.state[bus].map((trip) => {
               return(
                     <Trip key={trip.id} bus={bus} trip={trip} onSelectBus={this.handleBusSelection}/>
                );
            });
            return (
                <div id = {bus} className = {stripe} key={busCount++} onClick={this.handleMoveTrip}>
                    {bus_trips}
                </div>
            );
        });
        return (
            <div>
                {indBus}
            </div>
        );
    }
}
